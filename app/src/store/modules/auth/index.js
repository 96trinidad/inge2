/**
 * Auth Module
 */
import Vue from 'vue'
import Nprogress from 'nprogress';
import router from '../../../router';

const state = {
    user: localStorage.getItem('user')
}

// getters
const getters = {
    getUser: state => {
        return state.user;
    }
}

// actions
const actions = {
    login({commit}, payload){
        commit('onProcess');
        Vue.axios({
            method: 'POST',
            url: 'auth/login',
            data: {user: payload}
        }).then(res => {
            if(res.data.status === 'ok'){
                commit('loginUserSuccess', res.data.api_token)
            }
            else{
                commit('loginUserFailure', res.data.detail)
            }
        }).catch(error => {
            commit('loginUserFailure', error)
        })
    },
    logout: ({commit}) => {
        try {
            commit('logoutUser');
        } catch (error) {
            context.commit('loginUserFailure', error);
        }
        
    }
}

// mutations
const mutations = {
    onProcess(state) {
        Nprogress.start();
    },
    loginUserSuccess(state, token) {
        Nprogress.done();
        window.localStorage.setItem('_token', token)
        const jwtDecode = require('jwt-decode');
        state.user = jwtDecode(token).data;
        router.push('/');
        setTimeout(function(){
            Vue.notify({
                group: 'loggedIn',
                type: 'success',
                text: 'Usuario autenticado correctamente!'
            });
        },1500);
    },
    loginUserFailure(state, error) {
        Nprogress.done();
        Vue.notify({
            group: 'loggedIn',
            type: 'error',
            text: error
        });
    },
    logoutUser(state) {
        state.user = null;
        window.localStorage.removeItem('_token');
        router.push("/session/login");
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
