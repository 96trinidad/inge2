import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  modules: ['auth']
})

// modules
import auth from './modules/auth';
import settings from './modules/settings';
import sidebar from './modules/sidebar';

Vue.use(Vuex);
const debug = process.env.NODE_ENV !== 'production'
const url = (process.env.NODE_ENV !== 'production') ? 'http://localhost:8000/' : ''

export const store = new Vuex.Store({
    state: {
        apiURL: `${url}`,
    },
    strict: debug,
	plugins: [vuexLocal.plugin],
    modules: {
        auth,
        settings,
        sidebar
    }
})
