import Vue from 'vue'
import Router from 'vue-router'
import {store} from './../store/store'

//routes
import defaultRoutes from './default';

// session components
const Login = () => import('Views/session/Login');
const LockScreen = () => import('Views/session/LockScreen');
const ForgotPassword = () => import('Views/session/ForgotPassword');
const ResetPassword = () => import('Views/session/ResetPassword');

const Auth0CallBack = () => import('Components/Auth0Callback/Auth0Callback');

Vue.use(Router)


const router = new Router({
	mode: 'history',
	routes: [
		defaultRoutes,
		{
			path: '/callback',
			component: Auth0CallBack
		},
		{
			path: '/session/login',
			component: Login,
			meta: {
				title: 'message.login',
				breadcrumb: null
			}
		},
		{
			path: '/session/lock-screen',
			component: LockScreen,
			meta: {
				title: 'Lock Screen',
				breadcrumb: null
			}
		},
		{
			path: '/session/forgot-password',
			component: ForgotPassword,
			meta: {
				title: 'message.forgotPassword',
				breadcrumb: null
			}
		},
		{
			path: '/session/reset-password',
			component: ResetPassword,
			meta: {
				title: 'message.resetPassword',
				breadcrumb: null
			}
		}
	]
})

// navigation guards before each
router.beforeEach((to, from, next) => {

	//If the path is session login and the user is auth then go to home
	if(to.path === '/session/login' && store.state.auth.user){
		window.location.href= '/'
		return false
	}

	if (to.matched.some(record => record.meta.requiresAuth)) {
		// this route requires auth, check if logged in
		// if not, redirect to login page.
		if (localStorage.getItem('_token') === null) {
			next({
				path: '/session/login',
				query: { redirect: to.fullPath }
			})
		} else {
			next()
		}
	} else {
		next() // make sure to always call next()!
	}
})

// navigation guard after each
router.afterEach((to, from) => {
	setTimeout(() => {
		const contentWrapper = document.querySelector(".v-content__wrap");
		if(contentWrapper !== null){
			contentWrapper.scrollTop = 0;
		}
		const boxedLayout = document.querySelector('.app-boxed-layout .app-content');
		if(boxedLayout !==  null){
			boxedLayout.scrollTop = 0;
		}
		const miniLayout = document.querySelector('.app-mini-layout .app-content');
		if(miniLayout !== null){
			miniLayout.scrollTop = 0;
		}
	}, 200);
})

export default router
