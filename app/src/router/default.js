import Full from 'Container/Full'

// dashboard components
const Home = () => import('Views/dashboard/Home');

// users views
const UserProfile = () => import('Views/users/UserProfile');
const UsersList = () => import('Views/users/UsersList');

export default {
   path: '/',
   component: Full,
   redirect: '/default/dashboard/home',
   children: [
      {
         path: '/default/dashboard/home',
         component: Home,
         meta: {
            requiresAuth: true,
            title: 'message.home',
            breadcrumb: null
         }
      },
      // users
      {
         path: '/default/users/user-profile',
         component: UserProfile,
         meta: {
            requiresAuth: true,
            title: 'message.userProfile',
            breadcrumb:  [
              {
                breadcrumbInactive: 'Users /'
              },
              {
                breadcrumbActive: 'User Profile'
              }
            ]
         }
      },
      {
         path: '/default/users/users-list',
         component: UsersList,
         meta: {
            requiresAuth: true,
            title: 'message.usersList',
            breadcrumb: [
              {
                breadcrumbInactive: 'Users /'
              },
              {
                breadcrumbActive: 'Users List'
              }
            ]
         }
      },
   ]
}
