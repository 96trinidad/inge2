/**
 * App Entry File
 * Vuely: A Powerfull Material Design Admin Template
 * Copyright 2018. All Rights Reserved
 * Created By: The Iron Network, LLC
 * Made with Love
 */
import 'babel-polyfill';
import Vue from 'vue'
import Vuetify from 'vuetify'
import { Vue2Dragula } from 'vue2-dragula'
import VueQuillEditor from 'vue-quill-editor'
import wysiwyg from 'vue-wysiwyg'
import VueBreadcrumbs from 'vue2-breadcrumbs'
import Notifications from 'vue-notification'
import velocity from 'velocity-animate'
import AmCharts from 'amcharts3'
import AmSerial from 'amcharts3/amcharts/serial'
import AmAngularGauge from 'amcharts3/amcharts/gauge'
import Nprogress from 'nprogress'
import VueI18n from 'vue-i18n'
import VueTour from 'vue-tour'
import fullscreen from 'vue-fullscreen'
import InstantSearch from 'vue-instantsearch'
import VueVideoPlayer from 'vue-video-player';

// global components
import GlobalComponents from './globalComponents'

// app.vue
import App from './App'

// router
import router from './router'

// themes
import primaryTheme from './themes/primaryTheme';

// store
import { store } from './store/store';

// include script file
import './lib/VuelyScript'

// include all css files
import './lib/VuelyCss'

// messages
import messages from './lang';


// plugins
Vue.use(Vuetify, {
	theme: store.getters.selectedTheme.theme
});
Vue.use(InstantSearch);
Vue.use(VueI18n)
Vue.use(AmCharts)
Vue.use(AmSerial)
Vue.use(VueTour)
Vue.use(AmAngularGauge)
Vue.use(Vue2Dragula)
Vue.use(VueQuillEditor)
Vue.use(wysiwyg, {})
Vue.use(VueBreadcrumbs)
Vue.use(Notifications, { velocity })
Vue.use(fullscreen);
Vue.use(GlobalComponents);
Vue.use(VueVideoPlayer);

// Create VueI18n instance with options
const i18n = new VueI18n({
	locale: store.getters.selectedLocale.locale, // set locale
	messages, // set locale messages
})

//------ axios
import axios from 'axios'
import VueAxios from 'vue-axios'
axios.defaults.baseURL = store.state.apiURL
axios.interceptors.request.use((config) => {
	config.headers.common['Authorization'] = `${window.localStorage.getItem('_token')}`
	return config
})
axios.interceptors.response.use(response =>{
	return response;
}, error => {
	if(error.response.status === 401 && error.request.responseURL !== `${store.state.apiURL}auth/login`){
		store.dispatch('auth/logout').then(() => router.push('/session/login'))
	}else{
		return error.response;
	}
});
Vue.use(VueAxios, axios)
//------//

//------Filtros
Vue.filter('toUpperCaseWords', text => {
	const words = text.toLowerCase().split(' ')
	return words.map(word => word.charAt(0).toUpperCase()+word.slice(1)).join(' ')
})
Vue.filter('separator', (value, separator='.') => {
	return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator)
})
//------//


/* eslint-disable no-new */
new Vue({
	store,
	i18n,
	router,
	render: h => h(App),
	components: { App }
}).$mount('#app')
