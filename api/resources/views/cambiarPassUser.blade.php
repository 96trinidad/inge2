<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8" />
</head>
<body>
  <h2> Hola {{ $nombre }}</h2>
  <p>
    Para poder cambiar tu contraseña debes ingresar en el siguiente link:
    <a href="{{ env('APP_URL') }}/change-password/{{ $cod }}/{{ $id }}">Cambiar contraseña</a>
  </p>
</body>
</html>