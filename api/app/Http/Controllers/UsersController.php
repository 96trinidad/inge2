<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\User;
use Exception;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\BienvenidaUser;
use App\Mail\CambiarPassUser;

class UsersController extends Controller {

    public function new(Request $request){

        try 
        {

            //validate json request
            if(!$request->isJson())
            {
                return response()->json(['error'=>'Bad request'], 400);
            }

            //validation request
            $data = $request->input('user');
            $validator = Validator::make($data, [
                'nombre' => 'required',
                'apellido' => 'required',
                'email' => 'required|unique:users,email'
            ], [
                'required' => 'El atributo es requerido',
                'unique' => 'El atributo debe ser unico'
            ]);

            if($validator->fails()){
                return response()->json(['error' => 'Validation error', 'data' => $validator->errors()], 200);
            }

            //make username
            $username = strtolower($data['nombre'] . substr($data['apellido'], 0, 1));
            $aux = $username;
            //Verify if the username is available and change for another if necessary
            $count = 1;
            $userExist = false;
            do
            {
                if($userExist){
                    $username = $aux . $count;
                    $count++;
                }
                $userExist = User::where('user', $username)->exists();
                
            } while($userExist);
            
            //Generate random password
            $password = Str::random(8);

            //Save new user
            $newUser = new User;
            $newUser->nombre = strtolower($data['nombre']);
            $newUser->apellido = strtolower($data['apellido']);
            $newUser->email = strtolower($data['email']);
            $newUser->user = strtolower($username);
            $newUser->estado = 1;
            $newUser->password = Hash::make($password);
            $newUser->cod_confirmacion = str_random(32);
            $newUser->save();

            //Send email with the random password not encrypted
            Mail::to($data['email'])->send(new BienvenidaUser($newUser, $password));

            //Success response
            return response()->json(['status' => 'ok', 'data' => $newUser], 200);
            
        } catch (Exception $e) {
            return response()->json(['error' => 'Server error', 'data' => $e->getMessage()], 500);
        }

    }

    public function all(){
        try {
            $users = User::select()->where('estado', 1)->get();
            return response()->json(['status' => 'ok', 'users' => $users], 200);
        } catch (Exception $e) {
            return response()->json(['error' => 'Server error', 'data' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request){
        try {
            
            //validate json request
            if(!$request->isJson())
            {
                return response()->json(['error'=>'Bad request'], 400);
            }

            //validation request
            $data = $request->input('user');
            $validator = Validator::make($data, [
                'id' => 'required',
                'nombre' => 'required',
                'apellido' => 'required',
                'email' => 'required',
                'user' => 'required'
            ], ['required' => 'El atributo es requerido']);

            $emailExist = User::where([
                ['email', strtolower($data['email'])],
                ['id', '<>', $data['id']]
            ])->exists();

            $userExist = User::where([
                ['user', strtolower($data['user'])],
                ['id', '<>', $data['id']]
            ])->exists();

            $validator->after(function ($validator) use($emailExist, $userExist) {
                if($emailExist){
                    $validator->errors()->add('email', 'Email no disponible');
                }
                if($userExist){
                    $validator->errors()->add('user', 'User no disponible');
                }
            });

            if($validator->fails()){
                return response()->json(['error' => 'Validation error', 'data' => $validator->errors()], 200);
            }

            //Save new values
            $userUpdate = User::find($data['id']);
            if(!$userUpdate){
                return response()->json(['error' => 'Usuario no encontrado', 'data' => $validator->errors()], 200);
            }

            $userUpdate->nombre = strtolower($data['nombre']);
            $userUpdate->apellido = strtolower($data['apellido']);
            $userUpdate->email = strtolower($data['email']);
            $userUpdate->user = strtolower($data['user']);
            $userUpdate->save();

            //Success response
            return response()->json(['status' => 'ok', 'data' => $userUpdate], 200);

        } catch (Exception $e) {
            return response()->json(['error' => 'Server error', 'data' => $e->getMessage()], 500);
        }
    }

    public function changePass(Request $request){
        try {
            
            if(!$request->isJson()){
                return response()->json(['error' => 'Bad request'], 400);
            }

            //validation request
            $data = $request->input('user');
            $validator = Validator::make($data, [
                'password' => 'required',
                'password_repeat' => 'required|same:password'
            ], [
                'required' => 'El atributo es requerido',
                'same' => 'Debe coincidir con el password'
            ]);
            if($validator->fails()){
                return response()->json(['error' => 'Validation error', 'data' => $validator->errors()], 200);
            }

            //Find user
            $user = User::find($request->auth['id']);
            if(!$user){
                return response()->json(['error' => 'Validation error', 'data' => ['Usuario no encontrado']], 200);
            }

            //Save
            $user->password = Hash::make($data['password']);
            $user->save();

            //Success
            return response()->json(['status'=>'ok', 'data'=>['user' => $user]]);


        } catch (Exception $e) {
            return response()->json(['error' => 'Server error', 'data' => $e->getMessage()], 500);
        }
    }

    public function delete(Request $request){
        try {

            //Validation json request
            if(!$request->isJson()){
                return response()->json(['error'=>'Bad request'], 400);
            }

            //Validation request body
            $data = $request->input('user');
            $validator = Validator::make($data, ['id' => 'required|exists:users'], [
                'required' => 'El atributo es requerido',
                'exists' => 'Usuario no encontrado'
            ]);
            if($validator->fails()){
                return response()->json(['error' => 'Validation error', 'data' => $validator->errors()], 200);
            }

            //Find user and delete
            $userDelete = User::find($data['id']);
            $userDelete->fecha_baja = Carbon::now()->toDateTimeString();
            $userDelete->estado = 3;
            $userDelete->save();

            //Success response
            return response()->json(['status' => 'ok', 'data' => $userDelete], 200);

        } catch (\Throwable $e) {
            return response()->json(['error' => 'Server error', 'data' => $e->getMessage()], 500);
        }
    }

    public function forgoutPass(Request $request){
        try {
            
            if(!$request->isJson()){
                return response()->json(['error' => 'Bad request'], 400);
            }

            //validation request
            $data = $request->input('user');
            $validator = Validator::make($data, ['email' => 'required|exists:users'], [
                'required' => 'El atributo es requerido',
                'exists' => 'Usuario no encontrado'
            ]);
            if($validator->fails()){
                return response()->json(['error' => 'Validation error', 'data' => $validator->errors()], 200);
            }

            //Generate a cod confirmation
            $cod = str_random(32);

            //Get user
            $user = User::where('email', $data['email'])->first();
            $user->cod_confirmacion = $cod;
            $user->save();

            //Send email
            Mail::to($data['email'])->send(new CambiarPassUser($user));

            //Success response
            return response()->json(['status' => 'ok', 'data' => $user], 200);

        } catch (Exception $e) {
            return response()->json(['error' => 'Server error', 'data' => $e->getMessage()], 500);
        }
    }


    public function changePassMail(Request $request){
        try {
            
            if(!$request->isJson()){
                return response()->json(['error' => 'Bad request'], 400);
            }

            //validation request
            $data = $request->input('user');
            $validator = Validator::make($data, [
                'id' => 'required|exists:users',
                'password' => 'required',
                'password_repeat' => 'required|same:password',
                'cod' => 'required'
            ], [
                'required' => 'El atributo es requerido',
                'exists' => 'Usuario no encontrado',
                'same' => 'Debe coincidir con el password'
            ]);
            if($validator->fails()){
                return response()->json(['error' => 'Validation error', 'data' => $validator->errors(), 'request' => $data], 200);
            }

            //Find user
            $user = User::where([
                ['id', $data['id']],
                ['cod_confirmacion', $data['cod']]
            ])->first();
            if(!$user){
                return response()->json(['error' => 'Validation error', 'data' => ['Datos incorrectos']], 200);
            }

            //Save
            $user->password = Hash::make($data['password']);
            $user->save();

            //Success
            return response()->json(['status'=>'ok', 'data'=>['user' => $user]]);


        } catch (Exception $e) {
            return response()->json(['error' => 'Server error', 'data' => $e->getMessage()], 500);
        }
    }


    public function fieldVerify(Request $request){
        try {
            
            if(!$request->isJson()){
                return response()->json(['error' => 'Bad request'], 400);
            }

            //validation request
            $data = $request->all();
            $validator = Validator::make($data, [
                'field' => 'required',
                'value' => 'required'
            ], ['required' => 'El atributo es requerido']);
            if($validator->fails()){
                return response()->json(['error' => 'Validation error', 'data' => $validator->errors()], 200);
            }

            //Verify
            $row = User::
            query()
            ->where(function($builder) use($data){
               $builder->whereRaw("LOWER({$data['field']}) = ?", [$data['value']]);
               if(isset($data['exclude'])){
                   $builder->whereNotIn($data['exclude']['field'], [$data['exclude']['value']]);
               }
            })->exists();
            
            //Success
            return response()->json(['status'=>'ok', 'data'=>['exists' => $row]]);


        } catch (Exception $e) {
            return response()->json(['error' => 'Server error', 'data' => $e->getMessage()], 500);
        }
    }
}
