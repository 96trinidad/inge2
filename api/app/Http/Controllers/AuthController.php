<?php

namespace App\Http\Controllers;

use Validator;
use Exception;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController
{

    private $request;


    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    protected function jwt(User $user)
    {
        $payload = [
            'iss' => "lumen-jwt", //issuer o emisor
            'data' => $user->toArray(), //Subject
            'iat' => time(), //Time when JWT was issuer
            'exp' => time() + 60 * 60 * 24
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function authenticate(Request $request)
    {
        try {
            if(!$this->request->isJson())
            {
                return response()->json(['status' => 'error', 'detail' => 'Bad request'], 400);
            }

            $this->validate($this->request, [
                'user.email' => 'required|email',
                'user.password' => 'required'
            ]);

            $data = $request->input('user');

            $user = User::where('email', $data['email'])->first();

            if(!$user)
            {
                return response()->json([
                    'status' => 'error', 
                    'detail' => 'Credenciales incorrectos'
                ], 401);
            }

            if(Hash::check($data['password'], $user->password))
            {

                return response()->json([
                    'status' => 'ok',
                    'api_token' => $this->jwt($user)
                ], 200);
            }

            return response()->json(['status' => 'error', 'detail' => 'Credenciales incorrectos'], 200);

        } catch (Exception $e) {
            return response()->json(['error' => 'Server error', 'msg' => $e->getMessage()], 500);
        }

        
    }
}