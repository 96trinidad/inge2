<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class BienvenidaUser extends Mailable {
 
    use Queueable,
        SerializesModels;
    

    protected $user;
    protected $pass;


    public function __construct(User $user, $pass)
    {
        $this->user = $user;
        $this->pass = $pass;
    }

    //build the message.
    public function build() 
    {

        return $this->view('bienvenidaUser')
            ->with([
                'nombre' => $this->user->nombre,
                'apellido' => $this->user->apellido,
                'password' => $this->pass
            ]);
    }
}