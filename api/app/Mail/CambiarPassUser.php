<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class CambiarPassUser extends Mailable {
 
    use Queueable,
        SerializesModels;
    

    protected $user;


    public function __construct(User $user)
    {
        $this->user = $user;
    }

    //build the message.
    public function build() 
    {

        return $this->view('cambiarPassUser')
            ->with([
                'nombre' => $this->user->nombre,
                'apellido' => $this->user->apellido,
                'id' => $this->user->id,
                'cod' => $this->user->cod_confirmacion
            ]);
    }
}