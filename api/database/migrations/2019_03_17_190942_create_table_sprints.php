<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSprints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sprints', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->string('observacion', 500)->nullable();
            $table->unsignedBigInteger('id_proyecto');
            $table->timestamps();
        });

        Schema::table('sprints', function(Blueprint $table) {
            $table->foreign('id_proyecto')->references('id')->on('proyectos');
        });

        Schema::table('us', function(Blueprint $table) {
            $table->foreign('id_sprint')->references('id')->on('sprints');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sprints');
    }
}
