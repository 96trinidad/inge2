<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user', 15)->unique();
            $table->string('email', 20)->unique();
            $table->string('password', 100);
            $table->string('nro_documento', 20);
            $table->string('nombre', 30);
            $table->string('apellido', 30);
            $table->string('contacto', 20);
            $table->TinyInteger('estado', false)->default(1)->comment('1: activo, 2: inactivo 3:eliminado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
