<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProyectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 50);
            $table->date('fecha_inicio');
            $table->date('fecha_fin')->nullable();
            $table->TinyInteger('estado', false)->default(1)->comment('1: activo, 2: inactivo 3:eliminado');
            $table->unsignedBigInteger('id_equipo')->nullable();
            $table->timestamps();
        });

        Schema::table('proyectos', function(Blueprint $table) {
            $table->foreign('id_equipo')->references('id')->on('equipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
