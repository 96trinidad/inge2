<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableActividades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('detalles');
            $table->unsignedBigInteger('id_us');
            $table->unsignedBigInteger('id_usuario_asignado');
            $table->timestamps();
        });

        Schema::table('actividades', function(Blueprint $table) {
            $table->foreign('id_us')->references('id')->on('us');
            $table->foreign('id_usuario_asignado')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividades');
    }
}
