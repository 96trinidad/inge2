<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEstados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('detalle', 30);
            $table->unsignedBigInteger('id_actividad');
            $table->integer('orden', false);
            $table->decimal('horas_calculadas', 5, 2)->nullable();
            $table->decimal('horas_reales', 5, 2)->nullable();
            $table->timestamps();
        });

        Schema::table('estados', function(Blueprint $table) {
            $table->foreign('id_actividad')->references('id')->on('actividades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estados');
    }
}
