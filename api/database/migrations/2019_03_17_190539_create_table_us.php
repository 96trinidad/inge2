<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('us', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('detalle');
            $table->unsignedBigInteger('id_sprint')->nullable();
            $table->unsignedBigInteger('id_proyecto');
            $table->timestamps();
        });

        Schema::table('us', function(Blueprint $table) {
            $table->foreign('id_proyecto')->references('id')->on('proyectos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('us');
    }
}
