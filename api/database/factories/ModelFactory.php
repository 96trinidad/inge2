<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
use Illuminate\Support\Facades\Hash;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    $name = 'angel';
    $lastname = 'trinidad';
    $user = explode(' ',$name)[0] . substr($lastname, 0, 1);
    return [
        'user'=> $user,
        'nombre' => $name,
        'apellido'=>$lastname,
        'email' => '96trinidad@gmail.com',
        'password' => Hash::make('1234567'),
        'nro_documento' => '4047199',
        'contacto' => '0985447324'
    ];
});
