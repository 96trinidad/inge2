<?php

/*
|--------------------------------------------------------------------------
| application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post('auth/login', 'AuthController@authenticate');
$router->post('api/v1/forgoutPassUser', 'UsersController@forgoutPass');
$router->put('api/v1/changePassMailUser', 'UsersController@changePassMail');   
$router->group(['prefix' => 'api/v1', 'middleware' => 'auth'], function() use ($router){
    //Users
    $router->post('user', 'UsersController@new');
    $router->get('user', 'UsersController@all');
    $router->put('changePassUser', 'UsersController@changePass');
    $router->put('user', 'UsersController@update');
    $router->delete('user', 'UsersController@delete');
});